# Debian 8 Ansible Roles

## Basics

### hostname
* set hostname

### time
* set timezone
* ntp service

### langs
* set default locale
* generate locales

### apt
* apt https transport
* add backports repo
* remove source repos
* install basic tooling

### jre8
* install openjdk-8-jre from backports

### backup
* daily backup to s3 storage via duply
* gpg encrypted

### docker
* docker service
* docker registry login

### haproxy
* Proxy localhost:8080 by default
* Redirect http traffic to https

## Databases

### pxc: Percona XtraDB Cluster
* galera mysql synchronous multimaster replication
* mysql node service
* arbitrator node service

### mysql-backup
* daily xtrabackup hotcopy to local dir
* weekly mysqldump to local dir

### datomic
* datomic pro transactor service
* datomic pro console service
* daily backup to local dir

### datomic-storage
* create datomic mysql database/table
* create datomic mysql user

### rethinkdb
* rethinkdb service
* daily backup to local dir

## Services

### Artifactory
* Artifactory OSS version
* Bind to localhost only

